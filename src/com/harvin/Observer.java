package com.harvin;

//观察者的接口
public interface Observer {
	public void update();
}
