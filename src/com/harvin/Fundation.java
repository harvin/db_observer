package com.harvin;

import java.util.LinkedList;

//实现主题
public class Fundation implements Subject {
	LinkedList<Observer> observers = new LinkedList();
	private int attacker;
	
	public Fundation(){
		this(0);
	}
	
	public Fundation(int attacker){
		this.attacker = attacker;
	}
	
	public int get_attacker(){
		return attacker;
	}
	
	//收到攻击，发出警报
	public void under_attack(){
		notify_observer();
	}
	
	//注册观察者
	public void register_observer(Observer observer) {
		this.observers.add(observer);
	}
	//移除观察者
	public void remove_observer(Observer observer) {
		this.observers.remove(observer);
	}
	//调用观察者改变
	public void notify_observer() {
		for(int i=0; i<observers.size();i++){
			observers.get(i).update();
		}
	}

}
