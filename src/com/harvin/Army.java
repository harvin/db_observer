package com.harvin;

public class Army implements Observer {

	public void update() {
		System.out.println("Army: we are attacking the fundation.");
	}
	
	public void attack(Subject subject){
		if(subject instanceof Fundation){
			Fundation fundation = (Fundation) subject;
			fundation.under_attack();
		}
	}

}
