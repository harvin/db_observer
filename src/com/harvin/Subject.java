package com.harvin;

//主题的接口，定义了主题的主要功能
public interface Subject {
	public void register_observer(Observer observer);
	public void remove_observer(Observer observer);
	public void notify_observer();
}
