package com.harvin;

public class Player implements Observer {
	Subject subject;
	
	public Player(Subject subject){
		this.subject = subject;
		subject.register_observer(this);
	}
	
	public void update() {
		System.out.println("Player: the function is under attack.");
		//从主题中拉攻击者的数量
		if (subject instanceof Fundation) {
			Fundation fundation = (Fundation) subject;
			System.out.println(fundation.get_attacker()+
				" attacker(s) is attacking.");
		}
		
	}

}
