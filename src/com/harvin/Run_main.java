package com.harvin;

/*
 *  【观察者模式解释】
 *  类型：行为模式
 *	定义了一种一对多的依赖关系，让多个观察者对象同时监听某一个主题对象。
 *  这个主题对象的状态发生改变时，会通知所有的观察者对象，使它们能够自己更新自己。
 *
 *	举例：假设在一个游戏战役中，多名玩家共同保护一个基地。
 *  当基地受到攻击时，该基地旗下的所有玩家都会收到基地告急的信息，
 *  各玩家收到消息后，获取基地受攻击的人数。
 *	在该例子中，基地就是被监听的主题对象，而玩家就是观察者。
 *
 *	在这里，基地向玩家发送受攻击消息是“推”的模型。
 *	而“拉的”模型就是，玩家选择性获取基地受攻击的人数。
 */
public class Run_main {
	public static void main(String[] args){
		Fundation fundation = new Fundation(12);
		
		Player player1 = new Player(fundation);
		Player player2 = new Player(fundation);
		Player player3 = new Player(fundation);
		
		Army army	   = new Army();
		
		System.out.println("the army start attack the fundation.");
		army.attack(fundation);
		
		//注册攻击者到fundation的监听中
		fundation.register_observer(army);
		
		System.out.println("\n\nthe army start attack the fundation again.");
		army.attack(fundation);
	}
}
